﻿using Newtonsoft.Json;
using System;

namespace JsonExample
{
    class Program
    {
        static void Main(string[] args)
        {
            string json = @"{'Email': 'mail@example.com','Pass': '228' }";
            Account account = JsonConvert.DeserializeObject<Account>(json);
            Console.WriteLine(account.Email);
            Console.ReadKey();

        }
        public class Account
        {
            public string Email { get; set; }
            public string Pass { get; set; }
            
        }
    }
}
 
